//
//  TVTweetCell.m
//  twitterView
//
//  Created by Michael R Traverso on 3/21/14.
//  Copyright (c) 2014 traversoft. All rights reserved.
//

#import "TVTweetCell.h"

@implementation TVTweetCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
