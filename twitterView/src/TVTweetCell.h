//
//  TVTweetCell.h
//  twitterView
//
//  Created by Michael R Traverso on 3/21/14.
//  Copyright (c) 2014 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVTweetCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgUserPicture;
@property (strong, nonatomic) IBOutlet UILabel *lblTweet;

@end
