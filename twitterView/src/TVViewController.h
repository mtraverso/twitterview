//
//  TVViewController.h
//  twitterView
//
//  Created by Michael R Traverso on 3/20/14.
//  Copyright (c) 2014 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVViewController : UIViewController <FHSTwitterEngineAccessTokenDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UITableView *tblTweets;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)btnTouchUpInside:(id)sender;
- (void)storeAccessToken:(NSString *)accessToken;
- (NSString *)loadAccessToken;

@end
