//
//  TVViewController.m
//  twitterView
//
//  Created by Michael R Traverso on 3/20/14.
//  Copyright (c) 2014 traversoft. All rights reserved.
//

#import "TVViewController.h"
#import "TVTweetCell.h"

#define kTweetCellIdentifier @"TVTweetCell"

@interface TVViewController ()
{
    NSMutableArray *_userTweets;
}
@end

@implementation TVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _userTweets = [[NSArray alloc] init];
    [_btnLogin setHidden:NO];
    [_tblTweets setHidden:YES];
    [_tblTweets.layer setCornerRadius:4.0f];
    [_searchBar setHidden:YES];
    [[FHSTwitterEngine sharedEngine] setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FHSTwitterEngineAccessTokenDelegate
- (void)storeAccessToken:(NSString *)accessToken
{
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:kTwitterAccessToken];
}

- (NSString *)loadAccessToken
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:kTwitterAccessToken];
}

#pragma mark - Button handling
- (IBAction)btnTouchUpInside:(id)sender
{
    if (sender == _btnLogin)
    {
        UIViewController *twitterLoginController = [[FHSTwitterEngine sharedEngine] loginControllerWithCompletionHandler:^(BOOL success) {
            if (success)
            {
                [[FHSTwitterEngine sharedEngine] setAccessToken:[[FHSTwitterEngine sharedEngine] accessToken]];
                [self prepareForLoggedInUser];
            }
        }];
        [self presentViewController:twitterLoginController animated:YES completion:nil];
    }
}

- (void)prepareForLoggedInUser
{
    NSLog(@"Successfully logged into Twitter");
    [_btnLogin setHidden:YES];
    [_searchBar setHidden:NO];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    // Get user handle and retrieve last 100 tweets in timeline
    NSString *twitterHandle = [searchBar.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
    _userTweets = [NSMutableArray arrayWithArray:[[FHSTwitterEngine sharedEngine] getTimelineForUser:twitterHandle isID:NO count:100]];
    
    // Reload the table and scroll back to the top
    [_tblTweets reloadData];
    [_tblTweets scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    // Show the table if there's something to show
    if ([_userTweets count] > 0)
    {
        [self displayTableWithAnimation];
    }
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (void)displayTableWithAnimation
{
    [_tblTweets setAlpha:0.0f];
    [_tblTweets setHidden:NO];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [_tblTweets setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {  }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_userTweets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TVTweetCell *tweetCell = [tableView dequeueReusableCellWithIdentifier:kTweetCellIdentifier];
    if (tweetCell)
    {
        NSDictionary *tweetInfo = [_userTweets objectAtIndex:indexPath.row];
        NSString *url;
        if ([tweetInfo objectForKey:@"retweeted_status"]) {
            url = [[[tweetInfo objectForKey:@"retweeted_status"] objectForKey:@"user"] objectForKey:@"profile_image_url"];
        }
        else {
            url = [[tweetInfo objectForKey:@"user"] objectForKey:@"profile_image_url"];
        }
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]] queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
        {
            if (data)
            {
                UIImage *image = [UIImage imageWithData:data];
                if (image)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[tweetCell imgUserPicture] setImage:image];
                    });
                }
            }

        }];
        
        [[tweetCell lblTweet] setText:[tweetInfo objectForKey:@"text"]];
    }
    return  tweetCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
