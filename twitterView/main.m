//
//  main.m
//  twitterView
//
//  Created by Michael R Traverso on 3/20/14.
//  Copyright (c) 2014 traversoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TVAppDelegate class]));
    }
}
